package ostapchuk;

import scala.Tuple2;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.SparkSession;

import java.io.File;

public class Hockey {

    public static void main(String[] args) throws Exception {

        if (args.length != 3) {
            System.err.println("usage:" +
                    " Hockey <path to Master.csv> <path to AwardsPlayer.csv> masterURL(yarn)");
            System.exit(1);
        }

        String Master = args[0];
        String Awards = args[1];

        previousResultsCheck();

        SparkSession spark = SparkSession
                .builder()
                .appName("Hockey")
                .config("spark.master", args[2])
                .getOrCreate();

        JavaRDD<String> awardsFileAllData = spark.sparkContext().textFile(Awards, 1).toJavaRDD();

        JavaRDD<String> masterFileAllData = spark.sparkContext().textFile(Master, 1).toJavaRDD();

        JavaPairRDD<String, String> idAndName = masterFileAllData.mapToPair(s -> {
            String[] split = s.split(",");
            String id = split[0];
            String fullName = split[3] + split[4];
            return new Tuple2<>(id, fullName);
        });

        JavaRDD<String> onlyPlayers = awardsFileAllData.map(s -> {
            String[] splitted = s.split(",");
            return splitted[0];
        });

        JavaPairRDD<String, Integer> playerAndAward = onlyPlayers.mapToPair(s -> new Tuple2<>(s, 1));

        JavaPairRDD<String, Integer> counts = playerAndAward.reduceByKey((i1, i2) -> i1 + i2);

        JavaPairRDD<String, Tuple2<String, Integer>> joinedRDDs = idAndName.join(counts);

        JavaRDD<String> toSave = joinedRDDs.map(s -> (s._2._1 + " - " + s._2._2));

        toSave.foreach(s -> System.out.println(s));

        toSave.coalesce(1).saveAsTextFile("results");

        spark.stop();
    }

    private static void previousResultsCheck() {
        File previousResults = new File("results");
        if (previousResults.exists()) {
            deleteDirectory(previousResults);
        }
    }

    private static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }
}